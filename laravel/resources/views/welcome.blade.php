<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                background-color: #CCEEFF;
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
            }

            .content {
                text-align: center;
                display: inline-block;
            }
            .button {
                background-color: #000000;
                color: #FFFFFF;
                float: right;
                padding: 10px;
                border-radius: 10px;
                -moz-border-radius: 10px;
                -webkit-border-radius: 10px;
            }

            .small-btn {
                width: 50px;
                height: 20px;
            }

            .medium-btn {
                width: 70px;
                height: 30px;
            }

            .big-btn {
                width: 90px;
                height: 40px;
            }
            .title {
                font-size: 96px;
            }
            p.serif {
                font-family: "Times New Roman", Times, serif;
            }
            h1 {
                font-size: 250%;
            }
        </style>
    </head>
    <body>
        <div class="container">
                <div class="title">Login using Twitter account </div>
                <h1><p class="serif" >Username:</p></h1>
            <input size="50" type="text" name="Usename">
            <br>
            <h1><p class="serif" >Password:</p></h1>
            <input size="50" type="text" name="Password">
            <br><br>
            <input type="button" value="Login">
        </div>

    </body>
</html>
